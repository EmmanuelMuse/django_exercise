from django.db import models
from django_extensions.db.fields import CreationDateTimeField, ModificationDateTimeField

# Create your models here.

class MonitoredInterface(models.model): 
    external_id = models.AutoField(primary_key=True, verbose_name="External Resources unique identifier")
    mac_address_validator = RegexValidator(r'^[A-F0-9]')
    mac_address = models.CharField(max_length=12, validators=[mac_address_validator] ,verbose_name="MAC address")   
    ipv4_address = models.Charfield(max_length=200, verbose_name="ipv4 address")
    #Read the documentation and wanted to utilize the django_extensions CreateDateTimeField & ModificationDateTimeField
    created_at = CreationDateTimeField(verbose_name = "Creation Date & Time")
    updated_at = ModificationDateTimeField(verbose_name = "Last Update Date & Time")
    class Meta:
        unique_together = ('external_id', 'mac_address',)

    #_str_ might or might not be needed, i haven't decided yet
    def __str__(self):
        return '%s %s' % (self.external_id, self.mac_address)